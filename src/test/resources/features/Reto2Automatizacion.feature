#Author: czuluagag@choucairtesting.com

@tag
Feature: Orange HRM Demo automation

  @successfulCase
  Scenario: Login to the page as administrator and create a new user
    Given that Cyan needs to create an employee in the OrageHR
    When Cyan makes the registration entry in the application
    |firstName |middleName	 |lastName	|employeeId		|location 									 	|otherId	|dateOfBirth |maritalStatus		|driverLicense	|licenseExpiryDate|nationality	|nickName						|militaryService	|bloodGroup	|hobbies 					|
    |Violets 	 |Cradle	 		 |Star			|6699					|     Australian Regional HQ	|20160711	|2000-06-06	 |Single					|123456					|2030-12-12				|Colombian		|CunaDeEstrellas  	|NoAplica		   		|AB					|Videogames 			|
    And Cyan search for the created user
    |searchName 											|
    |Violets Cradle Star		   			 	|
    Then Cyan visualizes 6699 in the application
