package co.com.proyectobase.screenplay.stepdefinitions;

import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.model.RegisterEmployee;
import co.com.proyectobase.screenplay.questions.Correct;
import co.com.proyectobase.screenplay.tasks.AddEmployee;
import co.com.proyectobase.screenplay.tasks.Login;
import co.com.proyectobase.screenplay.tasks.Search;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class Challenge2StepDefinition {
	
	@Managed (driver="chrome")
	private WebDriver hisBrowser;
	
	private Actor cyan = Actor.named("Cyan");
	
	@Before
	public void initialSetup () {
		cyan.can(BrowseTheWeb.with(hisBrowser));
	}
	
	@Given("^that Cyan needs to create an employee in the OrageHR$")
	public void thatCyanNeedsToCreateAnEmployeeInTheOrageHR() throws Exception {
		cyan.wasAbleTo(Login.withAdministratorUser());
	 
	}

	@When("^Cyan makes the registration entry in the application$")
	public void cyanMakesTheRegistrationEntryInTheApplication(List<RegisterEmployee> datesFormOne) throws Exception {
		cyan.attemptsTo(AddEmployee.inTheFormOne(datesFormOne));
	}
	
	@When("^Cyan search for the created user$")
	public void cyanSearchForTheCreatedUser(List<RegisterEmployee> datesFormOne) throws Exception {
		cyan.attemptsTo(Search.inThePage(datesFormOne));
	}

	@Then("^Cyan visualizes (.*) in the application$")
	public void cyanVisualizesTheNewEmployeeInTheApplication(String employeeIdCorrect) throws Exception {
		cyan.should(GivenWhenThen.seeThat(Correct.employeeId(), Matchers.equalTo(employeeIdCorrect)));
	}
	
}
