package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.userinterface.OrangePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;

public class Login implements Task {

	private OrangePage orangePage;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(orangePage));
		actor.attemptsTo(Click.on(OrangePage.BOTON_LOGIN));
		actor.attemptsTo(Click.on(OrangePage.BOTON_PIM));
		actor.attemptsTo(Click.on(OrangePage.BOTON_ADD));
		try {
			Thread.sleep(15000);
		}
		catch(InterruptedException e) {
		}					
	}

	public static Login withAdministratorUser() {
		return Tasks.instrumented(Login.class);
	}

}
