package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.model.RegisterEmployee;
import co.com.proyectobase.screenplay.userinterface.OrangePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Search implements Task{
	
	private List<RegisterEmployee> registerEmployee;
	
	public Search(List<RegisterEmployee> registerEmployee) {
		super();
		this.registerEmployee = registerEmployee;
	}	
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.on(OrangePage.BOTON_EMPLOYEE_LIST));
		actor.attemptsTo(Click.on(OrangePage.SEARCH));
		actor.attemptsTo(Enter.theValue(registerEmployee.get(0).getSearchName()).into(OrangePage.SEARCH));
		actor.attemptsTo(Click.on(OrangePage.BOTON_SEARCH));
		try {
			Thread.sleep(15000);
		}
		catch(InterruptedException e) {
		}
	}

	public static Search inThePage(List<RegisterEmployee> datesSearch) {
		return Tasks.instrumented(Search.class, datesSearch);
	}

	
}
