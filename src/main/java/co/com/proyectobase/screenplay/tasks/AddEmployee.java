package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.interactions.SeleccionarLista;
import co.com.proyectobase.screenplay.model.RegisterEmployee;
import co.com.proyectobase.screenplay.userinterface.PageFormOne;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class AddEmployee implements Task{
	
	private List<RegisterEmployee> registerEmployee;
	
	public AddEmployee(List<RegisterEmployee> registerEmployee) {
		super();
		this.registerEmployee = registerEmployee;
	}
	
		
	@Override
	public <T extends Actor> void performAs(T actor) {
	
		actor.attemptsTo(Enter.theValue(registerEmployee.get(0).getFirstName()).into(PageFormOne.FIRT_NAME));
		actor.attemptsTo(Enter.theValue(registerEmployee.get(0).getMiddleName()).into(PageFormOne.MIDDLE_NAME));
		actor.attemptsTo(Enter.theValue(registerEmployee.get(0).getLastName()).into(PageFormOne.LAST_NAME));
		actor.attemptsTo(Click.on(PageFormOne.EMPLOYEE_ID));
		actor.attemptsTo(Enter.theValue(registerEmployee.get(0).getEmployeeId()).into(PageFormOne.EMPLOYEE_ID));
		actor.attemptsTo(Click.on(PageFormOne.LOCATION));
		actor.attemptsTo(Click.on("//span[contains(text(),'" +registerEmployee.get(0).getLocation()+ "')]"));
		actor.attemptsTo(Click.on(PageFormOne.SAVE_BUTTON));
		try {
			Thread.sleep(15000);
		}
		catch(InterruptedException e) {
		}
		actor.attemptsTo(Enter.theValue(registerEmployee.get(0).getOtherId()).into(PageFormOne.OTHER_ID));
		actor.attemptsTo(Enter.theValue(registerEmployee.get(0).getDateOfBirth()).into(PageFormOne.DATE_OF_BIRTH));
		actor.attemptsTo(Click.on(PageFormOne.MARITAL_STATUS));
		actor.attemptsTo(SeleccionarLista.Desde(PageFormOne.SELECT_MARITAL_STATUS, registerEmployee.get(0).getMaritalStatus().trim()));
		actor.attemptsTo(Click.on(PageFormOne.BUTTON_NATIONALITY));
		actor.attemptsTo(Click.on("//span[contains(text(),'" +registerEmployee.get(0).getNationality()+ "')]"));
		actor.attemptsTo(Click.on(PageFormOne.BUTTON_GENDER_MALE));
		actor.attemptsTo(Click.on(PageFormOne.BUTTON_GENDER_FEMALE));
		actor.attemptsTo(Enter.theValue(registerEmployee.get(0).getDriverLicense()).into(PageFormOne.DRIVER_LICENSE));
		actor.attemptsTo(Enter.theValue(registerEmployee.get(0).getLicenseExpiryDate()).into(PageFormOne.LICENSE_EXPIRY_DATE));
		actor.attemptsTo(Enter.theValue(registerEmployee.get(0).getNickName()).into(PageFormOne.NICK_NAME));
		actor.attemptsTo(Enter.theValue(registerEmployee.get(0).getMilitaryService()).into(PageFormOne.MILITARY_SERVICE));
		actor.attemptsTo(Click.on(PageFormOne.BUTTON_SAVE));
		actor.attemptsTo(Click.on(PageFormOne.BUTTON_BLOOD_GROUP));
		actor.attemptsTo(Click.on("//span[contains(text(),'" +registerEmployee.get(0).getBloodGroup().trim()+ "')]"));
		actor.attemptsTo(Enter.theValue(registerEmployee.get(0).getHobbies()).into(PageFormOne.HOBBIES));
		actor.attemptsTo(Click.on(PageFormOne.BUTTON_FINAL_SAVE));
		try {
			Thread.sleep(5000);
		}
		catch(InterruptedException e) {
		}
	}

	public static AddEmployee inTheFormOne(List<RegisterEmployee> registerEmployee) {
		
		return Tasks.instrumented(AddEmployee.class, registerEmployee);
	}
	
}
