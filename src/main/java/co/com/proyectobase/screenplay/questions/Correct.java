package co.com.proyectobase.screenplay.questions;

import co.com.proyectobase.screenplay.userinterface.OrangePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class Correct implements Question<String>{

	public static Correct employeeId() {
		
		return new Correct();
	}

	@Override
	public String answeredBy(Actor actor) {
		
		return Text.of(OrangePage.CHECK_CORRECT_ID).viewedBy(actor).asString();
	}

}
