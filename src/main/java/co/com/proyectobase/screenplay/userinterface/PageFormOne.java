package co.com.proyectobase.screenplay.userinterface;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class PageFormOne extends PageObject{

	public static final Target FIRT_NAME = Target.the("firstName").located(By.id("firstName"));
	public static final Target MIDDLE_NAME = Target.the("MiddleName").located(By.id("middleName"));
	public static final Target LAST_NAME = Target.the("LastName").located(By.id("lastName"));
	public static final Target LOCATION = Target.the("location").located(By.xpath("//*[@id=\"location_inputfileddiv\"]/div/input"));
	public static final Target SELECT_LOCATION = Target.the("SelectLocation").located(By.xpath("\"//*[@id=\\\"select-options-0417a8a6-bfce-9a59-07d9-78af60b4bb91\\\"]/li[3]/span\""));
	public static final Target SAVE_BUTTON = Target.the("SaveButton").located(By.id("systemUserSaveBtn"));
	public static final Target OTHER_ID = Target.the("otherId").located(By.id("other_id"));
	public static final Target DATE_OF_BIRTH = Target.the("dateOfBirth").located(By.id("date_of_birth"));
	public static final Target MARITAL_STATUS = Target.the("maritalStatus").located(By.xpath("//*[@id=\"marital_status_inputfileddiv\"]/div/input"));
	public static final Target SELECT_MARITAL_STATUS = Target.the("maritalStatus").located(By.xpath("//*[@id='marital_status_inputfileddiv']/div/ul[contains(@id,'select-options')]"));
	public static final Target BUTTON_NATIONALITY = Target.the("BUTTON_NATIONALITY").located(By.xpath("//*[@id=\"nationality_inputfileddiv\"]/div/input"));
	public static final Target SELECT_NATIONALITY = Target.the("SELECT_NATIONALITY").located(By.id("select-options-51933fa5-2fac-3bc6-be33-84f0808af521"));
	public static final Target BUTTON_GENDER_MALE = Target.the("BUTTON_GENDER_MALE").located(By.xpath("//*[@id=\"pimPersonalDetailsForm\"]/materializecss-decorator[3]/div/sf-decorator[3]/div/ul/li[1]/label"));
	public static final Target BUTTON_GENDER_FEMALE = Target.the("BUTTON_GENDER_FEMALE").located(By.xpath("//*[@id=\"pimPersonalDetailsForm\"]/materializecss-decorator[3]/div/sf-decorator[3]/div/ul/li[2]/label"));
	public static final Target DRIVER_LICENSE = Target.the("DRIVER_LICENSE").located(By.id("driver_license"));
	public static final Target LICENSE_EXPIRY_DATE = Target.the("LICENSE_EXPIRY_DATE").located(By.id("license_expiry_date"));
	public static final Target NICK_NAME = Target.the("NICK_NAME").located(By.id("nickName"));
	public static final Target MILITARY_SERVICE = Target.the("MILITARY_SERVICE").located(By.id("militaryService"));
	public static final Target BUTTON_SAVE = Target.the("BUTTON_SAVE").located(By.xpath("//*[@id=\"pimPersonalDetailsForm\"]/materializecss-decorator[8]/div/sf-decorator[2]/div/button"));
	public static final Target BUTTON_BLOOD_GROUP = Target.the("BUTTON_BLOOD_GROUP").located(By.xpath("//*[@id=\"1_inputfileddiv\"]/div/input"));
	public static final Target SELECT_BLOOD_GROUP = Target.the("SELECT_BLOOD_GROUP").located(By.xpath("//*[@id=\"select-options-4deeed40-7f52-c7d8-8652-c02a98826834\"]"));
	public static final Target HOBBIES = Target.the("HOBBIES").located(By.xpath("//*[@id=\"5\"]"));
	public static final Target BUTTON_FINAL_SAVE = Target.the("BUTTON_FINAL_SAVE").located(By.xpath("//*[@id=\"content\"]/div[2]/ui-view/div[2]/div/custom-fields-panel/div/form/materializecss-decorator[3]/div/sf-decorator/div/button"));
	public static final Target EMPLOYEE_ID = Target.the("EMPLOYEE_ID").located(By.id("employeeId"));
}
