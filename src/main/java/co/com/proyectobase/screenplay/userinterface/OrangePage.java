package co.com.proyectobase.screenplay.userinterface;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl ("https://orangehrm-demo-6x.orangehrmlive.com/")
	public class OrangePage extends PageObject{
	public static final Target BOTON_LOGIN = Target.the("BOTON_LOGIN").located(By.id("btnLogin"));
	public static final Target BOTON_PIM = Target.the("Boton_PIM").located(By.xpath("//*[@id=\"menu_pim_viewPimModule\"]/a/span[2]"));
	public static final Target BOTON_ADD = Target.the("BOTON_ADD_EMPLOYEE").located(By.xpath("//*[@id=\"menu_pim_addEmployee\"]/span[2]"));
	public static final Target BOTON_EMPLOYEE_LIST = Target.the("BOTON_EMPLOYEE_LIST").located(By.xpath("//*[@id=\"menu_pim_viewEmployeeList\"]/span[2]"));
	public static final Target SEARCH = Target.the("Search").located(By.id("employee_name_quick_filter_employee_list_value"));
	public static final Target BOTON_SEARCH = Target.the("BOTON_SEARCH").located(By.id("quick_search_icon"));
	public static final Target CHECK_CORRECT_ID = Target.the("").located(By.xpath("//*[@id=\"employeeListTable\"]/tbody/tr/td[2]"));
}
